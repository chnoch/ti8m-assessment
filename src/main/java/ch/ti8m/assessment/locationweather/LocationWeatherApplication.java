package ch.ti8m.assessment.locationweather;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LocationWeatherApplication {

	public static void main(String[] args) {
		SpringApplication.run(LocationWeatherApplication.class, args);
	}

}
